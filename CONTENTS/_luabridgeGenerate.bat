::::::::::::::::::::::::::::::
::::::LuaBridgeGenerator::::::
:::::::::::::2017:::::::::::::
::Copyright © Mathew Aloisio::
::::::::::::::::::::::::::::::

@echo off

:: Configuration (Edit this if neccesary.)
set SEARCH_DIR[1]="%~dp0Source"
set SEARCH_DIR[2]="C:\Program Files (x86)\Steam\steamapps\common\Leadwerks\Include"
set SEARCH_DIR.length=2

::::::::::::::DO NOT EDIT CODE UNDER THIS LINE::::::::::::::

:: Generate argument string.
setlocal EnableExtensions EnableDelayedExpansion
set ARGS=
for /L %%i in (1,1,%SEARCH_DIR.length%) do set ARGS=!ARGS! !SEARCH_DIR[%%i]!

:: LuaBridge binding generation.
cd .\LuaBridge\
LuaBridgeGenerator.exe!ARGS!

:: LuaBridge source movement.
::if not exist "..\Source\LuaBridge\" mkdir "..\Source\LuaBridge\"
::move /Y "luabridge_bindings.h" "..\Source\LuaBridge\luabridge_bindings.h"
::move /Y "luabridge_bindings.cpp" "..\Source\LuaBridge\luabridge_bindings.cpp"

pause
