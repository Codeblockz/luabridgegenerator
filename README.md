﻿# Windows only!

# Installation Instructions

  1. Open the 'CONTENTS' folder.
  2. (If not already there.) Open the 'LuaBridgeGenerator.sln' file with Visual Studio (2017) and compile the program.
  3. Drag 'LuaBridgeGenerator.exe' into the 'CONTENTS' folder.
  4. Open '_luabridgeGenerate.bat' and ensure all your source code directories are a part of the 'SEARCH_DIR' array, ensure 'SEARCH_DIR.length' is correct.

  >**IMPORTANT NOTICE:** Directories added to the 'SEARCH_DIR' array ***MUST*** not end in a trailing backslash '\' as this would cause the trailing quotation mark to be handled incorrectly.
  
# Usage Instructions

  1. Execute '*_luabridgeGenerate.bat*' located in your projects main directory.
  2. Open your project in any IDE and add the generated source files to your project. (Files: luabridge_bindings.h, luabridge_bindings.cpp)
  > Your generated source files can be found in '*CONTENTS/LuaBridge/*'.
  3. Open the generated 'luabridge_bindings.cpp' and search for <CHANGEME>.h, replace this with the include(s) required by the generated '.cpp' file as the generator does not (currently) do this for you! 
  4. Include '*tolua_export.h*' in a cpp file and execute 'LuaBridge_Open(lua_State* L), the argument being the desired lua state to bind to.

# Not supported
 - Overloaded methods. (As LuaBridge does not support these.)
 - Method defintions spread amongst multiple lines. (As LuaBridgeGenerator does not currently support this.)
 
# Credits
 - ToLuaPkgGenerator - Mathew Aloisio
 - LuaBridge - https://github.com/vinniefalco/LuaBridge

# Known Bugs
  > Currently none.
  
# Development

Want to contribute? Great!
However, please do not repost your changes on another repository, instead submit them as pull requests.