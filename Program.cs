﻿/*----------------------------------/
/--------LuaBridgeGenerator---------/
/--------------2017-----------------/
/----Copyright © Mathew Aloisio-----/
/----------------------------------*/

using System;
using System.IO;
using System.Collections.Generic;
#if DEBUG
using System.Diagnostics;
#endif

namespace LuaBridgeGenerator {
    class Program {
        static bool IsValidMemberString(string pIn) {
            // Invalid if 'operator' keyword is found.
            if (pIn.Contains("operator"))
                return false;

            return true;
        }

        static void FindLua(string pPath) {
            if (!File.Exists(pPath)) {
                Console.WriteLine("WARNING: Failed to open non-existant file \"" + pPath + "\"");
                return;
            }
            try {
                using (StreamReader stream = new StreamReader(File.OpenRead(pPath))) {
                    string currentNamespace = "";
                    TClass cppClass = null;
                    while (stream.Peek() >= 0) {
                        string line = stream.ReadLine().Trim();
                        if (line.Length == 0) continue;
                        if (!line.Contains("using") && line.Contains("namespace")) {
                            string[] strings = line.Split(' ');
                            currentNamespace = strings[1];
                            continue;
                        }
                        string[] lineStrings = line.Split(new string[] { "//" }, StringSplitOptions.None);
                        if (lineStrings.Length != 0) {
                            string tag = lineStrings[lineStrings.Length - 1].Trim();
                            if (tag.ToLower() == "lua") {
                                line = lineStrings[0].Trim();
                                string[] strings = line.Split(' ');   
                                bool containsClass = line.Contains("class");
                                if (containsClass || line.Contains("struct")) {
                                    // Class declaration.
                                    cppClass = new TClass(strings[1], currentNamespace);        
                                    cppClass.type = containsClass ? TClassType.Class : TClassType.Struct;
                                    if (strings.Length > 4)
                                        cppClass.parentName = strings[4].Trim();
                                }
                                else {
                                    // Ensure the member is valid.
                                    string member = lineStrings[lineStrings.Length - 2].Trim();
                                    if (!IsValidMemberString(member))
                                        continue;

                                    if (cppClass != null) {
                                        // Class member.
                                        cppClass.members.Add(member);
                                    }
                                    else {
                                        // Namespace member.
                                        if (!Classless.members.ContainsKey(currentNamespace))
                                            Classless.members.Add(currentNamespace, new List<string>());
                                        Classless.members[currentNamespace].Add(member);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception) {
                Console.WriteLine("EXCEPTION:" + exception);
#if DEBUG
                StackTrace trace = new StackTrace(exception, true);
                Console.WriteLine("\tFILE:" + trace.GetFrame(0).GetFileName());
                Console.WriteLine("\tLINE: " + trace.GetFrame(0).GetFileLineNumber());
#endif
            }
        }

        static void ReadHeaders(string pDir) {
            string[] headers = Directory.GetFiles(pDir, "*.h", SearchOption.AllDirectories);
            foreach (string header in headers) {
                FindLua(header);
            }
        }

        static void Main(string[] pArgs) {
            string directory = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) + "/";
            Console.WriteLine("[LuaBridgeGenerator] Generating LuaBridge bindings...");

            // Read all header files and gather class information.
            Console.WriteLine("[LuaBridgeGenerator] Reading information from header files...");
            ReadHeaders(directory);
            foreach (string argument in pArgs) {
                if (!Directory.Exists(argument)) {
                    Console.WriteLine("ERROR: Invalid directory \"" + argument + "\". Skipping directory.");
                    continue;
                }
                ReadHeaders(argument);
            }

            try {
                // Open the luabridge_bindings.cpp file for writing.
                string packageFilePath = "luabridge_bindings.cpp";
                Console.WriteLine("[LuaBridgeGenerator] Generating \"" + packageFilePath + "\"...");
                if (File.Exists(packageFilePath))
                    File.Delete(packageFilePath);
                using (StreamWriter stream = new StreamWriter(File.Create(packageFilePath))) {
                    // Write include & namespace header.
                    stream.WriteLine(
                        "// *This file was generated automatically by LuaBridgeGenerator.*" 
                        + System.Environment.NewLine +
                        "// *Copyright © 2017 Mathew Aloisio*"
                    );
                    stream.WriteLine("#include \"luabridge_bindings.h\"" + System.Environment.NewLine);
                    stream.WriteLine("/* Add includes here. */");
                    stream.WriteLine("#include \"<CHANGEME>.h\"");
                    stream.WriteLine(System.Environment.NewLine + "void LuaBridge_Open(lua_State* L) {");
                    stream.WriteLine("\tgetGlobalNamespace(L)");

                    // Traverse over all namespaces.
                    foreach (var pair in TClass.map) {
                        // Write namespace.
                        if (pair.Key.Length != 0)
                            stream.WriteLine("\t\t.beginNamespace(\"" + pair.Key + "\")");

                        // Write namespace members.
                        if (Classless.members.ContainsKey(pair.Key))
                            Classless.WriteMembers(stream, pair.Key);

                        // TClass pass #1 (Assign parents.)
                        foreach (TClass cppClass in pair.Value.Values) {
                            if (cppClass.parentName.Length != 0) {
                                cppClass.parent = TClass.Find(cppClass.parentName, pair.Key);
                            }
                        }

                        // TClass pass #2 (Write luabridge file.)
                        foreach (TClass cppClass in pair.Value.Values) {
                            cppClass.Write(stream);
                        }

                        // Write end-of-namespace.
                        if (pair.Key.Length != 0)
                            stream.WriteLine("\t\t.endNamespace();");
                    }
                    stream.WriteLine("}");
                }
            }
            catch (Exception exception) { Console.WriteLine(exception); }

            // Write export header.
            string headerFilePath = "luabridge_bindings.h";
            Console.WriteLine("[LuaBridgeGenerator] Generating \"" + headerFilePath + "\"...");
            if (File.Exists(headerFilePath))
                File.Delete(headerFilePath);
            using (StreamWriter stream = new StreamWriter(File.Create(headerFilePath))) {
                stream.WriteLine("// LuaBridge Export Header");
                stream.WriteLine(
                    "// *This file was generated automatically by LuaBridgeGenerator.*"
                    + System.Environment.NewLine +
                    "// *Copyright © 2017 Mathew Aloisio*"
                );
                stream.WriteLine("#ifndef _LUABRIDGE_EXPORT_HEADER_");
                stream.WriteLine("#define _LUABRIDGE_EXPORT_HEADER_");
                stream.WriteLine("#pragma once");
                stream.WriteLine("#include \"lua.h\"");
                stream.WriteLine("#include \"LuaBridge/LuaBridge.h\"" + System.Environment.NewLine);
                stream.WriteLine("void LuaBridge_Open(lua_State* L);");
                stream.WriteLine("#endif");
            }
            Console.WriteLine("[LuaBridgeGenerator] Done generating LuaBridge bindings!");
        }
    }
}