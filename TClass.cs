﻿/*----------------------------------/
/--------LuaBridgeGenerator---------/
/--------------2017-----------------/
/----Copyright © Mathew Aloisio-----/
/----------------------------------*/

using System;
using System.IO;
using System.Collections.Generic;

namespace LuaBridgeGenerator {
    public enum TClassType {
        Class,
        Struct
    }

    public class Extractor {
        static public string ExtractTypeName(string pIn) {
            // 1st try to find a '=' sign.
            int rightIndex = pIn.LastIndexOf('=');
            if (rightIndex == -1) // No = sign, find ;
                rightIndex = pIn.LastIndexOf(";");
            if (rightIndex != -1) {
                int farIndex = -1;
                for (int i = rightIndex - 1; i >= 0; --i) {
                    if (farIndex == -1 && pIn[i] != ' ') {
                        farIndex = i + 1;
                    }
                    else if (farIndex != -1 && (pIn[i] == ' ' || i == 0)) {
                        int startIndex = i == 0 ? i : i + 1;
                        return pIn.Substring(startIndex, farIndex - startIndex);
                    }
                }
            }

            return "-ERROR-TYPE-";
        }

        static public string ExtractMethodName(string pIn) {
            // Find the last '('.
            int rightIndex = pIn.LastIndexOf('(');
            if (rightIndex != -1) {
                int farIndex = -1;
                for (int i = rightIndex - 1; i >= 0; --i) {
                    if (farIndex == -1 && pIn[i] != ' ') {
                        farIndex = i + 1;
                    }
                    else if (farIndex != -1 && (pIn[i] == ' ' || i == 0)) {
                        int startIndex = i == 0 ? i : i + 1;
                        return pIn.Substring(startIndex, farIndex - startIndex);
                    }
                }
            }

            return "-ERROR-METHOD-";
        }
    }

    public class Classless {
        public static Dictionary<string, List<string>> members = new Dictionary<string, List<string>>();

        public static void WriteMembers(StreamWriter pStream, string pNamespace) {
            // Determine member type.
            foreach (string member in members[pNamespace]) {
                if (member.Contains("(") && member.Contains(")")) {
                    // Type: Function.
                    string methodName = Extractor.ExtractMethodName(member);
                    string methodStr = pNamespace.Length == 0 ? methodName : pNamespace + "::" + methodName;
                    pStream.WriteLine("\t\t.addFunction(\"" + methodName + "\", &" + methodStr + ")");
                }
                else {
                    // Type: Variable.
                    string variableName = Extractor.ExtractTypeName(member);
                    string variableStr = pNamespace.Length == 0 ? variableName : pNamespace + "::" + variableName;
                    pStream.WriteLine("\t\t.addData(\"" + variableName + "\", &" + variableStr + ")");
                }
            }
        }
    }

    public class TClass {
        // Member(s)
        public string name;
        public string nameSpace;
        public TClass parent;
        public string parentName;
        public TClassType type;
        public List<string> members;

        private bool written;
        private string padding;

        // Static Member(s)
        static public Dictionary<string, Dictionary<string, TClass>> map = new Dictionary<string, Dictionary<string, TClass>>();

        // Constructor/Destructor
        public TClass(string pName, string pNamespace) {
            name = pName;
            nameSpace = pNamespace;
            parent = null;
            parentName = "";
            padding = pNamespace.Length == 0 ? "\t\t" : "\t\t\t";
            members = new List<string>();
            if (!map.ContainsKey(pNamespace))
                map.Add(pNamespace, new Dictionary<string, TClass>());
            if (!map[pNamespace].ContainsKey(pName))
                map[pNamespace].Add(pName, this);
        }

        // Methods
        private void WriteMember(StreamWriter pStream, string pMember) {
            // Determine member type.
            string methodName = Extractor.ExtractMethodName(pMember);
            string variableName = Extractor.ExtractTypeName(pMember);
            if (pMember.Split(' ')[0].Contains(name) && methodName == name) { // Just incase someone accidentally tagged a destructor.
                // Type: Constructor.
                pStream.WriteLine(padding + "\t.addConstructor<void(*)(" + GetArgumentsString(pMember) + ")>()");
            }
            else if (pMember.Contains("static") && (pMember.Contains("(") && pMember.Contains(")"))) {
                // Type: Static Function.
                string methodStr = nameSpace.Length == 0 ? name + "::" + methodName : nameSpace + "::" + name + "::" + methodName;
                pStream.WriteLine(padding + "\t.addStaticFunction(\"" + methodName + "\", &" + methodStr + ")");
            }
            else if(pMember.Contains("(") && pMember.Contains(")")) {
                // Type: Function.
                string methodStr = nameSpace.Length == 0 ? name + "::" + methodName : nameSpace + "::" + name + "::" + methodName;
                pStream.WriteLine(padding + "\t.addFunction(\"" + methodName + "\", &" + methodStr + ")");
            }
            else if (pMember.Contains("static")) {
                // Type: Static Variable.
                string variableStr = nameSpace.Length == 0 ? name + "::" + variableName : nameSpace + "::" + name + "::" + variableName;
                pStream.WriteLine(padding + "\t.addStaticData(\"" + variableName + "\", &" + variableStr + ")");
            }
            else {
                // Type: Variable.
                string variableStr = nameSpace.Length == 0 ? name + "::" + variableName : nameSpace + "::" + name + "::" + variableName;
                pStream.WriteLine(padding + "\t.addData(\"" + variableName + "\", &" + variableStr + ")");
            }
        }

        public void Write(StreamWriter pStream) {
            if (written)
                return;

            TClass baseClass = parent;
            while (baseClass != null) { // Write parent classes & their parents.
                baseClass.Write(pStream);
                baseClass = baseClass.parent;
            }

            string className = nameSpace.Length == 0 ? name : nameSpace + "::" + name;
            if (parentName.Length != 0) { 
                // Write derived class.
               pStream.WriteLine(padding + ".deriveClass<" + className + ", " + parentName + ">(\"" + name + "\")");
            }
            else {
                // Write class.
                pStream.WriteLine(padding + ".beginClass<" + className + ">(\"" + name + "\")");
            }
            foreach (string member in members) {
               WriteMember(pStream, member);
            }

            // End class.
            pStream.WriteLine(padding + ".endClass()");
            written = true;
        }

        // Static Methods
        static public TClass Find(string pName, string pNamespace) {
            return (map.ContainsKey(pNamespace) && map[pNamespace].ContainsKey(pName)) ? 
                map[pNamespace][pName] : null;
        }

        static void RemoveAssignment(ref string pIn) {
            int index = pIn.IndexOf('=');
            if (index == -1) // No equals sign.
                return;
            pIn = pIn.Substring(0, index).Trim();
        }

        static string GetArgumentsString(string pIn) {
            int indexStart = pIn.IndexOf('(');
            int indexEnd = pIn.LastIndexOf(')');
            if (indexStart == (indexEnd - 1)) // Skip (); style arguments.
                return "";
            if (indexStart != -1 && indexEnd != -1) {
                ++indexStart; // Exclude opening brace.
                string[] arguments = pIn.Substring(
                    indexStart,
                    indexEnd - indexStart).Trim().Split(',');
                for (int i = 0; i < arguments.Length; ++i) {
                    RemoveAssignment(ref arguments[i]);
                }
                return string.Join(",", arguments);
            }

            return "";
        }
    }
}
